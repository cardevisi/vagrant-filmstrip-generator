exec{"apt-get":
    command => "apt-get update"
}

exec{"ffmpeg":
    command => "sudo add-apt-repository ppa:mc3man/trusty-media",
    command => "sudo apt-get update",
    command => "sudo apt-get dist-upgrade"
}